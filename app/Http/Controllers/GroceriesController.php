<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grocery;
use App\Models\Category;
use App\Http\Requests\GroceryRequest;

class GroceriesController extends Controller
{
   //
   public function index()
   {
      $groceries = Grocery::with("category")->get();
      //$categories = Category::find(1)->categories;
      return view('/index', ['groceries' => $groceries]);
   }
   public function create()
   {
      return view('/create', ['categories' => Category::all()]);
   }
   public function store(Request $request)
   {
      $groceries = new Grocery();
      $groceries->name = $request->name;
      $groceries->price = $request->price;
      $groceries->amount = $request->amount;
      $groceries->category_id = $request->category_id;
      $groceries->save();  //save the request by de hand of de model

      return redirect('/');
   }
   public function edit(Grocery $grocery,)
   {
      return view('/edit', ['groceries' => $grocery, 'categories' => Category::all()]);
   }
   public function update(GroceryRequest $request, Grocery $grocery)
   {
      $grocery->name =  $request->get('name');  //Getting values from the request of the database grocery
      $grocery->price = $request->get('price');
      $grocery->amount = $request->get('amount');
      $grocery->category_id = $request->get('category_id');
      $grocery->save();
      return redirect()->route('groceries.index');
   }
   public function destroy(Grocery $grocery)
   {
      $grocery->delete();
      return redirect('/');
   }
};
