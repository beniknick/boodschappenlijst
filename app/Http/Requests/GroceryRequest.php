<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroceryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|required|min:2|max:55',
            'price' => ['required', 'regex:/^\d+(\,\d{1,2}|.\d{1,2})?$/'],
            'amount' => 'required|integer|gt:0',
            'publish_at' => 'nullable|date',
        ];
    }
}
