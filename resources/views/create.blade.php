@extends('master')

@section('title', 'Homepage')

@section('content')

<h1>Add to your Groceries</h1>

<form method="post" action="/store">
   <input type="text" name="name" placeholder="Item name">
   <input type="text" name="price" placeholder="Item price">
   <input type="text" name="amount" placeholder="Amount">
   <select type="text" name="category_id">
      @foreach ($categories as $category)
      <option value="{{$category->id}}">{{$category->category}}</option>
      @endforeach
   </select>
   {{csrf_field()}}<br>
   <button type="submit">Add</button>
</form>


@endsection