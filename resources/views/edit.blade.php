@extends('master')

@section('title', 'Homepage')

@section('content')

<h1>Edit Your Grocery item</h1>

@if ($errors->any())
<div class="alert alert-danger">
   <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
   </ul>
</div>
@endif

<form method="post" action="/update/{{$groceries->id}}">
   <input type="text" name="name" value=<?= $groceries->name ?>>
   <input type="text" name="price" value=<?= $groceries->price ?>>
   <input type="text" name="amount" value=<?= $groceries->amount ?>>
   <select type="text" name="category_id">
      @foreach ($categories as $category)
      <option value="{{$category->id}}">{{$category-> category}}</option>
      @endforeach
   </select>

   {{csrf_field()}}<br>
   <button type="submit">Save</button>
</form>


@endsection