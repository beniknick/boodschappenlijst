@extends('master')

@section('title', 'Homepage')

@section('content')

<h1>Your Grocery list</h1>

<div class="list">

   <table>
      <tr>
         <th>Name</th>
         <th>Category</th>
         <th>Price</th>
         <th>Aantal</th>
         <th>Total</th>
      </tr>
      @foreach($groceries as $grocery)
      <tr>
         <td>{{$grocery->name}}</td>
         <td>{{$grocery->category->category}}</td>
         <td>{{$grocery->price}}</td>
         <td>{{$grocery->amount}}</td>
         <td>{{$grocery->amount * $grocery->price}}</td>
         <td><a href="{{route('groceries.edit', $grocery->id)}}">Edit</a></td>
         <td><a href="{{route('groceries.destroy', $grocery->id)}}">Remove</a></td>
      </tr>
      @endforeach
   </table>

</div>

@endsection