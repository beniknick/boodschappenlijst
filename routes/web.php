<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GroceriesController;
use App\Models\Grocery;

//URL path is '/Groceries', action is 'index', route Name is 'groceries.index'
Route::get('/', [
   GroceriesController::class,
   'index'
])->name('groceries.index');

Route::get('/add', [
   GroceriesController::class,
   'create'
])->name('groceries.create');

Route::post('/store', [
   GroceriesController::class,
   'store'
])->name('groceries.store');

Route::get('/edit/{grocery}', [
   GroceriesController::class,
   'edit'
])->name('groceries.edit');

Route::post('/update/{grocery}', [
   GroceriesController::class,
   'update'
])->name('groceries.update');

Route::get('/destroy/{grocery}', [
   GroceriesController::class,
   'destroy'
])->name('groceries.destroy');


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
